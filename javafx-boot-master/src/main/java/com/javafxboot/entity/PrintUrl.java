package com.javafxboot.entity;

import lombok.Data;

@Data
public class PrintUrl {
    //打印url
    private String url;
    //渠道
    private String channel;
    //是否打印
    private Boolean isPrint;
    //订单号
    private String orderSn;
}
