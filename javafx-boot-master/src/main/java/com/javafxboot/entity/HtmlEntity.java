package com.javafxboot.entity;

import lombok.Data;

@Data
public class HtmlEntity {
    //打印html
    private String html;
    //渠道
    private String channel;
    //是否打印
    private Boolean isPrint;
    //订单号
    private String orderSn;
}
