package com.javafxboot.entity;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Data
public class OrdCod extends PrintUrl implements Serializable {

    /** 订单编号 */
    private String orderSn;

    /** 订单状态 */
    private String orderStatus;

    /** 国家地区 */
    private String country;

    private Long userId;

    /** 系统订单状态 */
    private String sysOrderStatus;

    /** pdf文件 */
    private String pdf;

    private List<OrdCodItemsDto> items;

    private BigDecimal collection;

    /** 目的地 */
    private String destination;

    /** 收件人 */
    private String recipientName;

    /** 收件地址 */
    private String shippingAddress;

    /** 收件电话 */
    private String recipientPhone;

    /** 收件邮编 */
    private String postcode;

    /** 收件省州 */
    private String provinceState;

    /** 收件城市 */
    private String city;

    /** HS海关编码 */
    private String hsCustomsCode;

    /** 单价 */
    private BigDecimal unitPrice;

    /** 申报 */
    private String declares;

    private String channel;

}
