package com.javafxboot.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class OrdCodItemsDto implements Serializable {
    private Long id;

    /** 商品图片 */
    private String images;

    /** 对应库存sku */
    private String numberSku;

    /** 对应库存数量 */
    private Integer numberCount;

    /** 订单编号 */
    private String orderSn;

    /** 库存图片 */
    private String stockImg;

    /** 库存名字 */
    private String stockName;

    /** 货架号 */
    private String shelvesCode;

    /** 图片 */
    private String imgUrl;

    private Long userId;

}
