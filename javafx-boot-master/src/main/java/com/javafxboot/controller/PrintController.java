package com.javafxboot.controller;

import com.javafxboot.Application;
import com.javafxboot.entity.HtmlEntity;
import com.javafxboot.entity.OrdCod;
import com.javafxboot.entity.PrintUrl;
import com.javafxboot.exception.BadRequestException;
import com.javafxboot.util.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

@RestController
@CrossOrigin
@Slf4j
public class PrintController {

    /**
     * pdf
     */
    @PostMapping("/url")
    public String pdf(@RequestBody PrintUrl printUrl) throws Exception{

            if("萊爾富".equals(printUrl.getChannel())){
                try {
                    Lef.readPdf(printUrl);
                }catch (Exception e){
                    //打印控制台堆栈信息，做为对比
                    log.error("e.printStackTrace()控制台异常堆栈信息：");
                    e.printStackTrace();
                    //method 1
                    log.error(collectExceptionStackMsg(e));
                    log.error(e.getMessage());
                }

            }else if("7-11".equals(printUrl.getChannel())){
                Tw711.readPdf(printUrl);
            }else if("全家".equals(printUrl.getChannel())){
                Qj.readPdf(printUrl);
            }else if("OK Mart".equals(printUrl.getChannel())){
                Ok.readPdf(printUrl);
            }else if("蝦皮宅配".equals(printUrl.getChannel())){
                ReversePdf.readPdf(printUrl);
            }else{
                DownPdf.readPdf(printUrl);
            }
            return "200";
    }

    /**
     * pdf
     */
    @PostMapping("/cod")
    public String pdf(@RequestBody OrdCod ordCod) throws Exception{
        Cod.readPdf(ordCod);
        return "200";
    }



    /**
     * 收集异常堆栈信息
     * */
    public static String collectExceptionStackMsg(Exception e){
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw, true));
        String strs = sw.toString();
        return strs;
    }


    /**
     * pdf
     */
    @PostMapping("/url/path")
    public String pdfPath(@RequestBody PrintUrl printUrl)  throws Exception{

            File directory = new File("");//参数为空
            String courseFile = directory.getCanonicalPath() ;
            System.out.println(courseFile);
            return courseFile;


    }

    /**
     * pdf
     */
    @PostMapping("/url/html")
    public String html(@RequestBody HtmlEntity data)  throws Exception{

            PdfPublic.readPdf(data);
            return "200";


    }

}
