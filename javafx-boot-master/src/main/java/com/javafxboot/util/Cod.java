package com.javafxboot.util;

import com.javafxboot.entity.OrdCod;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.geom.Dimension2D;
import java.awt.geom.Point2D;
import java.awt.print.*;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.spire.pdf.FileFormat;
import com.spire.pdf.PdfDocument;
import com.spire.pdf.PdfPageBase;
import com.spire.pdf.barcode.PdfCode128BBarcode;
import com.spire.pdf.barcode.TextLocation;
import com.spire.pdf.graphics.PdfMargins;
import com.spire.pdf.graphics.PdfPen;
import com.spire.pdf.graphics.PdfRGBColor;
import com.spire.pdf.graphics.PdfSolidBrush;
import com.spire.pdf.graphics.PdfTrueTypeFont;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
import org.apache.pdfbox.printing.PDFPrintable;
import org.apache.pdfbox.printing.Scaling;

public class Cod {
    /**
     * 传入一个.pdf 地址
     * @param data 地址
     * @throws Exception
     */
    public static void readPdf(OrdCod data) throws Exception {
        //创建PdfDocument对象
        PdfDocument doc = new PdfDocument();

        Dimension2D dimension2D = new Dimension();
        dimension2D.setSize(283,283);


        //设置新文档第一页的页边距为左右50，上下100
        PdfMargins margins = new PdfMargins(0, 0);
        PdfPageBase page = doc.getPages().add(dimension2D, margins);
        PdfPen pen =new PdfPen(new PdfRGBColor(Color.black),1);

        //外层框架点
        Point2D p_1_1 = new Point2D.Float(10,15); Point2D p_1_2 = new Point2D.Float(270,15);
        Point2D p_2_1 = new Point2D.Float(10,100); Point2D p_2_2 = new Point2D.Float(270,100);
        Point2D p_3_1 = new Point2D.Float(10,130); Point2D p_3_2 = new Point2D.Float(270,130);
        Point2D p_4_1 = new Point2D.Float(10,210); Point2D p_4_2 = new Point2D.Float(270,210);
        Point2D p_5_1 = new Point2D.Float(10,270); Point2D p_5_2 = new Point2D.Float(270,270);

        //横线
        page.getCanvas().drawLine(pen, p_1_1, p_1_2);
        page.getCanvas().drawLine(pen, p_2_1, p_2_2);
        page.getCanvas().drawLine(pen, p_3_1, p_3_2);
        page.getCanvas().drawLine(pen, p_4_1, p_4_2);
        page.getCanvas().drawLine(pen, p_5_1, p_5_2);

        //内4点
        Point2D p_2_3 = new Point2D.Float(100,100); Point2D p_2_4 = new Point2D.Float(190,100);
        Point2D p_3_3 = new Point2D.Float(100,130); Point2D p_3_4 = new Point2D.Float(190,130);

        //竖线
        page.getCanvas().drawLine(pen, p_1_1, p_5_1);
        page.getCanvas().drawLine(pen, p_1_2, p_5_2);
        page.getCanvas().drawLine(pen, p_2_3, p_3_3);
        page.getCanvas().drawLine(pen, p_2_4, p_3_4);


        PdfTrueTypeFont font13 = new PdfTrueTypeFont(new Font("宋体", Font.PLAIN,13),true);
        PdfTrueTypeFont font11 = new PdfTrueTypeFont(new Font("宋体", Font.PLAIN,11),true);
        PdfTrueTypeFont font8 = new PdfTrueTypeFont(new Font("宋体", Font.PLAIN,8),true);
        PdfSolidBrush brush = new PdfSolidBrush(new PdfRGBColor(Color.black));

        page.getCanvas().drawString("TO(ADDRESSEE)", font13, brush, new Point2D.Float(15, 15));
        page.getCanvas().drawString("QP集運便:"+data.getOrderSn(), font13, brush, new Point2D.Float(120, 15));

        page.getCanvas().drawString(data.getRecipientName(), font11, brush, new Point2D.Float(30, 32));
        page.getCanvas().drawString(data.getShippingAddress(), font11, brush, new Point2D.Float(30, 46));
        page.getCanvas().drawString(data.getProvinceState()+" "+data.getCity(), font11, brush, new Point2D.Float(30, 60));
        page.getCanvas().drawString(data.getPostcode() + " tel:"+data.getRecipientPhone(), font11, brush, new Point2D.Float(30, 74));


        page.getCanvas().drawString(data.getChannel(), font11, brush, new Point2D.Float(15, 108));
        page.getCanvas().drawString("G.W: 1.0", font11, brush, new Point2D.Float(105, 108));
        page.getCanvas().drawString("PCS: 1", font11, brush, new Point2D.Float(195, 108));

        page.getCanvas().drawString("Item:", font13, brush, new Point2D.Float(15, 130));
        page.getCanvas().drawString("COD: "+data.getCollection(), font13, brush, new Point2D.Float(150, 130));

        for(int i = 0; i<data.getItems().size(); i++) {
            page.getCanvas().drawString(data.getItems().get(i).getStockName()+ " / "+data.getItems().get(i).getNumberSku()+"*"+data.getItems().get(i).getNumberCount(), font8, brush, new Point2D.Float(20, 145+ i*10));
        }


        //PdfCode11Barcode
        PdfCode128BBarcode  codebarHct= new PdfCode128BBarcode (data.getOrderSn());
        codebarHct.setBarcodeToTextGapHeight(1f);
        Point2D.Float point = new Point2D.Float();
        PdfRGBColor blue = new PdfRGBColor(Color.BLACK);
        //codebarHct.setNarrowBarWidth(0.5f);
        codebarHct.setBarHeight(30f);
        codebarHct.setEnableCheckDigit(true);
        codebarHct.setShowCheckDigit(true);
        codebarHct.setTextDisplayLocation(TextLocation.Bottom);
        codebarHct.setTextColor(blue);
        point.setLocation(20,220);
        codebarHct.draw(page,point);


        StringBuffer imgFilePath = null;

        String imgFilePathPrefix = "";
        imgFilePath = new StringBuffer();
        imgFilePath.append(imgFilePathPrefix);
        Date dNow = new Date( );
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy\\MM\\dd\\");
        imgFilePath.append(ft.format(dNow));
        imgFilePath.append(data.getOrderSn()+".pdf");
        //目录不存在则创建
        isexitsPath(""+ft.format(dNow)+"\\p");
        //保存文档

        doc.saveToFile(imgFilePath.toString(), FileFormat.PDF);

        if(data.getIsPrint()){
            defaultPrintPDF(imgFilePath.toString());
        }

    }

    public static void defaultPrintPDF(String pdfPath) throws IOException, PrinterException {

        File file = new File(pdfPath);
        // 读取pdf文件
        PDDocument document = PDDocument.load(file);
        // 创建打印任务
        PrinterJob job = PrinterJob.getPrinterJob();

        job.setPageable(new PDFPageable(document));

        Paper paper = new Paper();
        // 设置打印纸张大小
        paper.setSize(283,283); // 1/72 inch
        // 设置打印位置 坐标
        paper.setImageableArea(0, -15, paper.getWidth(), paper.getHeight()); // no margins
        // custom page format
        PageFormat pageFormat = new PageFormat();
        pageFormat.setPaper(paper);
        // override the page format
        Book book = new Book();
        // append all pages 设置一些属性 是否缩放 打印张数等
        book.append(new PDFPrintable(document, Scaling.SCALE_TO_FIT), pageFormat, 1);
        job.setPageable(book);
        // 开始打印
        job.print();

    }

    public static boolean isexitsPath(String path)throws InterruptedException{
        String [] paths=path.split("\\\\");
        StringBuffer fullPath=new StringBuffer();
        for (int i = 0; i < paths.length; i++) {
            fullPath.append(paths[i]).append("\\\\");
            File file=new File(fullPath.toString());
            if(paths.length-1!=i){//判断path到文件名时，无须继续创建文件夹！
                if(!file.exists()){
                    file.mkdir();
                    System.out.println("创建目录为："+fullPath.toString());
                }
            }
        }
        File file=new File(fullPath.toString());//目录全路径
        if (!file.exists()) {
            return true;
        }else{
            return false;
        }
    }
}
