package com.javafxboot.util;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.LinkedList;

public class MergePdf {

    public static LinkedList<File> folderMethod(String path) {

        File file = new File(path);
        LinkedList<File> list = new LinkedList<>();

        //保存所有pdf文件的对象
        LinkedList<File> pdfList = new LinkedList<File>();

        //该路径对应的文件或文件夹是否存在
        if (file.exists()) {

            //如果该路径为---文件或空文件夹
            if (null == file.listFiles()) {
//        	System.out.println(file.getAbsolutePath());
                if(file.getAbsolutePath().endsWith(".pdf"))
                    pdfList.add(file);
            }

            //如果该路径为非空文件夹
            else {
                //将该路径下的所有文件（文件或文件夹）对象加入队列
                list.addAll(Arrays.asList(file.listFiles()));
                //遍历该队列
                while (!list.isEmpty()) {

                    File firstF = list.removeFirst();

                    //这里不论是文件夹还是文件，只需判断是否以“.pdf”结尾
                    if(firstF.getAbsolutePath().endsWith(".pdf") && firstF.getAbsolutePath().indexOf("-x") == -1)
                        pdfList.add(firstF);

                    File[] files = firstF.listFiles();

                    if (null == files) {
                        //System.out.println(firstF.getAbsolutePath());
                        continue;
                    }
                    for (File f : files) {
                        if (f.isDirectory()) {
                            //System.out.println("文件夹:" + f.getAbsolutePath());
                            list.add(f);
                        } else {
                            //System.out.println("文件:" + f.getAbsolutePath());

                            if(f.getAbsolutePath().endsWith(".pdf") && firstF.getAbsolutePath().indexOf("-x") == -1)
                                pdfList.add(f);

                        }
                    }
                }
            }

        } else {
            System.out.println("文件不存在!");
        }

        //输出所有pdf文件的路径
        for(File f : pdfList)
            System.out.println(f.getAbsolutePath());
        return pdfList;
    }




    /*
     * 合并pdf文件
     * @param files 要合并文件数组(绝对路径如{ "e:\\1.pdf", "e:\\2.pdf" ,
     * "e:\\3.pdf"}),合并的顺序按照数组中的先后顺序，如2.pdf合并在1.pdf后。
     * @param newfile 合并后新产生的文件绝对路径，如 e:\\temp\\tempNew.pdf,
     * @return boolean 合并成功返回true；否则，返回false
     *
     */

    public static boolean mergePdfFiles(LinkedList<File> files, String newfile) {
        boolean retValue = false;
        Document document = null;
        try {

            document = new Document(new PdfReader(files.get(0).getAbsolutePath()).getPageSize(1));
            PdfCopy copy = new PdfCopy(document, new FileOutputStream(newfile));
            document.open();
            for (int i = 0; i < files.size(); i++) {
                PdfReader reader = new PdfReader(files.get(i).getAbsolutePath());
                int n = reader.getNumberOfPages();
                for (int j = 1; j <= n; j++) {
                    document.newPage();
                    PdfImportedPage page = copy.getImportedPage(reader, j);
                    copy.addPage(page);
                }
            }
            retValue = true;
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            document.close();
        }
        return retValue;
    }
}
