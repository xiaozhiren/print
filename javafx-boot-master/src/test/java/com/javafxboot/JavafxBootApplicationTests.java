package com.javafxboot;

import com.javafxboot.entity.PrintUrl;
import com.javafxboot.util.Lef;
import com.javafxboot.util.Ok;
import com.javafxboot.util.Qj;
import com.javafxboot.util.Tw711;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JavafxBootApplicationTests {

    @Test
    public void contextLoads() throws Exception {
        PrintUrl data = new PrintUrl();
        //711
         //data.setUrl("https://partner.shopeemobile.com/api/v1/logistics/download_shipping_document?token=cUNidUZZY2Z0NDlQSEd5crmi4reXQXDcFS8QWHJqO952sSFlSW3dd2Zn7sQgcCK1N2GprV5I2v%2F0JzKudwMi0VFTZx%2B5Sm3ZvjFfm%2FVoVbpXlVFAU56LkEmbA3mRYsMr76wetsTO1ToPwNEJuavSep82L7u3PlmltHy%2FqT2bOetvWKo68SxJ23bb2vEBykMPFuXwKlzGCQ7bi5nGvP4Te1lxdDwUd2e7dSQSRJGk6dDy%2BasTzBoBOCPIOTk15Iv9GQfChaEKXBAqmt8weU2p1Qh0fb1USGu9Kefsoc2Rto%2BkoL4fAk8fPWaMy1XpBhnHHH%2BlI5ja40Nkb8D8j%2BhYAXDWg4nBKMOddfhoWejh09s%3D");

        //lef
        data.setUrl("https://partner.shopeemobile.com/api/v1/logistics/download_shipping_document?token=cUNidUZZY2Z0NDlQSEd5csxQjWnG%2BC51GRzsyMZ0E6Vnojo4M8jcNE06%2BQCMnz%2Fgt6q8HPTBOcyZgOhgkjcTauBfNGYEIyFHq4KeFbbMbvbOkjxrmWrf%2F%2B%2BVGlGodAtSSE5HbaCxIn9ZpAv1lLbXjkKRPPJMdkD2DbId1d7oOyLF9p%2FfYRghIWPuL%2B3A26dz0ZV0oxZI6fKjxaA8FwUTF%2FH4pntMnP0BunCuHXUBgICMWIDBFI8A03e3kvcEOWEreSoS8R6ABMyDE59ggvnJPKFc2xQ571NmjtMpbVIf2N9ydbLNe66Khv%2BxMihzrBbg3OX%2FdzzEEIj4swvmt7tHjTz9UHZiam%2B017zGZ7%2B0yTo%3D");

        //qj
        //data.setUrl("https://partner.shopeemobile.com/api/v1/logistics/download_shipping_document?token=cUNidUZZY2Z0NDlQSEd5coompjUuxlNEPUv9TciPBRLjXSSaMmqMpM7rZW%2FpbaRNhnjdZSpZbyEuyelN84CF6wtUDIOLlBcnwEEkMh0ErgELxNm8xuxbevxnm%2FCmaRoeQ%2FZ3vrOf9SBxh2nHQHc9ul7eLPnCkCWOFqJSrUzaTj6tjnfqJIFR22dm9LwwY%2Bonw9UyM7dRVy%2BGRyBFnXF2mxKA%2F33XquETaPivpByrdpdDimdJxw440x7wkQjMxOANT3PTNRNJdIBDCD91Ccli3Ovr2eoe1X%2BnoVMX5QX5NHqwI1zDo6%2F90PxxvVYuPr1orEYWF7lEv9fXXcvB72jXJRXhLa0MmgqBP7vcQ9H%2FnOw%3D");

        //ok
        //data.setUrl("https://partner.shopeemobile.com/api/v1/logistics/download_shipping_document?token=cUNidUZZY2Z0NDlQSEd5clr5U5OBqmo1lzrB3TKvZeTXLwq0n0pHe2UGNTUe9w9DTo2BR4cjY0%2BsQKpGeGRuinOxv62ELicPoyeMqZB0yOYyXVxJkhh%2FJCYjGNvs3LLlzW56v6%2Bup5FbbPd0HrYU65Xt7jOqaailgPvCKzmavH7%2FJEDP9gMgWlA6uM8eanPZkvmdrq56nsr02hSTKb4Zu%2B%2Be4I%2BDOlKXQz%2F%2BLFKiZhOeXK0pCahPpRssCKtPvGHc4n3F9D8i%2Fk9wJs4AfWATWlRFphysPDW2AKF8n%2FvpzWqQVIXPtiYrwP1QjU%2BnOe1CgrYwidueAsqRORVjiSb8R7jOmk3RdLw0hokEWPf014s%3D");

        data.setChannel("1");
        data.setOrderSn("12345");
        data.setIsPrint(true);
        //Tw711.readPdf(data);1

        Lef.readPdf(data);

        //Qj.readPdf(data);

        //Ok.readPdf(data);
    }

}
